public with sharing class NewObjectController {
    public NewObjectController() {

    }

    public static NewObject__c getNewObjects() {
        List<NewObject__c> newObjects = [Select id from NewObject__c];
        return newObjects[0];
    }    
}
